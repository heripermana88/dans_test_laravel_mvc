<?php
namespace App\Services;

use App\Traits\ConsumeExternalService;

class jobService{
	use ConsumeExternalService;

	public function __construct()
	{
		$this->baseUri = env('JOB_URI');
		$this->baseUriDetail = env('JOB_DETAIL_URI');
	}

	public function fetchJobs($url_params=null)
	{
		return $this->performRequest('GET',$this->baseUri.$url_params);
	}

	public function detailJob($id=null)
	{
		return $this->performRequest('GET',$this->baseUriDetail.$id);
	}

}
