<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Display login Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginForm()
    {
        if(Auth::check()){
            return redirect()->route('home');
        }
        return view('login');
    }

    /**
     * Post params login
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
	    $rules = [
		    'email'	=>	'required|email',
		    'password'	=> 	'required|string'
	    ];


        $message = [
            'email.required' => 'isi field Email',
            'email.email' => 'email tidak Valid',
            'password.required' => 'isi field password',
        ];

	    $validator = Validator::make($request->all(),$rules, $message);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        
        $data = $request->only(['email','password']);
        
        Auth::attempt($data);
        if(Auth::check()){
            return redirect()->route('home');
        }else{
            Session::flash('error', 'Email dan password salah');
            return redirect()->route('home');
        }
    }

    /**
     * Display form Register.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerForm()
    {
        return view('register');
    }

    /**
     * Store new User Register.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:4|max:50',
		    'email'	=>	'required|email|unique:users,email',
		    'password'	=> 	'required|string|confirmed'
	    ];

        $message = [
            'name.required' => 'isi field Name',
            'name.min' => 'minimal 4 char',
            'name.max' => 'maximal 50 char',
            'email.required' => 'isi field Email',
            'email.email' => 'email tidak Valid',
            'email.unique' => 'email sudah terpakai',
            'password.required' => 'isi field password',
            'password.confirmed' => 'password confirm tidak sama',
        ];

        $validator = Validator::make($request->all(),$rules, $message);

	    if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }else{
            $user = new User();
            $user->name     = ucwords(strtolower($request->name));
            $user->email    = strtolower($request->email);
            $user->password = Hash::make($request->password);
            $register = $user->save();
        
            if($register){
                Session::flash('success', 'Register Succes!');
                return redirect()->route('login');
            }else{
                Session::flash('errors', 'Register Gagal!');
                    return redirect()->route('register');
            }
        }

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
