<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\jobService;

class JobController extends Controller
{
	public $jobService;


	public function __construct(jobService $jobService)
	{
		$this->jobService = $jobService;
	}	
	
	public function index(Request $request)
	{
		$url_params='';
		if(!isset($request->description)){
			$request->request->remove('description');
		}
		if(!isset($request->location)){
			$request->request->remove('location');
		}
		if(count($request->all())>0){
			$url_params = '?'.http_build_query($request->all());
		}
		
		$data = json_decode($this->jobService->fetchJobs($url_params),TRUE);

		return view('job',compact('data'));
	}

	public function detailJob($id){
		$data = json_decode($this->jobService->detailJob($id),TRUE);
		return view('job-detail',compact('data'));
	}
}
