<?php

namespace App\Traits;

use GuzzleHttp\Client;

trait ConsumeExternalService
{
	public function performRequest($method, $requestUrl, $formParams=[],$header=[])
	{
		$client = new Client([
			'base_uri' => $this->baseUri
		]);

		$response = $client->request($method, $requestUrl);
		return $response->getBody()->getContents();
	}
}
