<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Home</title>
    <style>
        .job-title{
            color:#037bfc;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="col-md-12">
            <div class="nav">
                <a href="{{ route('jobs') }}">back</a>
            </div>
            <br>
            <br>
            <font style="font-size:12px;"><b>{!! $data['type'] !!} / {!! $data['location'] !!}</b></font>
            <h3 class="job-title">
                {!! $data['title'] !!}
            </h3>
            <div class="row">
                <div class="col-md-8">
                    {!! $data['description'] !!}
                </div>
                <div class="col-md-4">
                    <img src="{!! $data['company_logo'] !!}" alt="" srcset="" class="col-md-12">
                    
                    <div class="card" style="margin-top:20px;">
                        <div class="card-header">
                            <h3 class="text-center">How to Apply</h3>
                        </div>
                        <div class="card-body">
                            {!! $data['how_to_apply'] !!}
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>
</body>
</html>