<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Register</title>
</head>
<body>
    <div class="container">
        <div class="col-md-4 offset-md-4 mt-5">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center">Register Form</h3>
                </div>
                <form action="{{ route('register') }}" method="post">
                    <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <label for=""><strong>Name</strong></label>
                                <input type="text" name="name" id="name" class="form-controller" required>
                            </div>
                            <div class="form-group">
                                <label for=""><strong>Email</strong></label>
                                <input type="email" name="email" id="email" class="form-controller" required>
                            </div>
                            <div class="form-group">
                                <label for=""><strong>Password</strong></label>
                                <input type="password" name="password" id="password" class="form-controller" required>
                            </div>
                            <div class="form-group">
                                <label for=""><strong>Password Confirm</strong></label>
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-controller" required>
                            </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('login') }}">login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>