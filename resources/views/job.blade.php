<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Home</title>
    <style>
        .job-title{
            color:#037bfc;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center">Jobs List</h3>
                </div>
                <div class="search">
                    <form action="/jobs" method="get">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="">Job Description</label>
                                <input type="text" name="description" id="description" value="{{ isset($_GET['description']) ? $_GET['description'] : ''}}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Location</label>
                                <input type="text" name="location" id="location" value="{{ isset($_GET['location']) ? $_GET['location'] : ''}}">
                            </div>
                            <div class="form-group col-md-4">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
                @foreach($data as $d)
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 text-left">
                            <strong class='job-title'><a href="detailJob/{{ $d['id'] }}">{{$d['title']}}</a></strong><br>
                            <strong class="type">{{$d['type']}}</strong>
                        </div>
                        <div class="col-md-4" style="text-align:right;">
                            <font class='location'>{{$d['location']}}</font><br>
                            <strong class="created_at">{{ \Carbon\Carbon::parse($d['created_at'])->diffForHumans() }}</strong>
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach
                <div class="card-footer text-center">
                    <a href="{{ route('logout') }}">Logout</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>